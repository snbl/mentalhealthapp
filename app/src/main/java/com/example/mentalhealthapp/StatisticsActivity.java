package com.example.mentalhealthapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class StatisticsActivity extends AppCompatActivity {

    LineGraphSeries<DataPoint> series;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        connectXMLViews();
    }
    GraphView graph = (GraphView) findViewById(R.id.graph);

    /*series = new LineGraphSeries<DataPoint>();
    for
    );
    graph.addSeries(series);*/

    private void connectXMLViews(){
        dateTextView = findViewById(R.id.date);
        waterTextView = findViewById(R.id.water_text);
        sleepTextView = findViewById(R.id.sleep_text);
        idealWaterTextView = findViewById(R.id.ideal_water_text);
        idealSleepTextView = findViewById(R.id.ideal_sleep_text);
    }

    //XMLViews
    private TextView dateTextView;
    private TextView waterTextView;
    private TextView sleepTextView;
    private TextView idealWaterTextView;
    private TextView idealSleepTextView;


}