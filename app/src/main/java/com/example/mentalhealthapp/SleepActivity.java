package com.example.mentalhealthapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class SleepActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep);

        connectingXMLViews();

    }

    private void connectingXMLViews() {
        sleepMeter1 = findViewById(R.id.hour_box_start);
        sleepMeter2 = findViewById(R.id.hour_box_2);
        sleepMeter3 = findViewById(R.id.hour_box_3);
        sleepMeter4 = findViewById(R.id.hour_box_4);
        sleepMeter5 = findViewById(R.id.hour_box_5);
        sleepMeter6 = findViewById(R.id.hour_box_6);
        sleepMeter7 = findViewById(R.id.hour_box_7);
        sleepMeter8 = findViewById(R.id.hour_box_8);
        sleepMeter9 = findViewById(R.id.hour_box_9);
        sleepMeter10 = findViewById(R.id.hour_box_10);
        sleepMeter11 = findViewById(R.id.hour_box_11);
        sleepMeter12 = findViewById(R.id.hour_box_12);
        sleepMeter13 = findViewById(R.id.hour_box_13);
        sleepMeter14 = findViewById(R.id.hour_box_end);
    }

    //XMLs
    private ImageButton sleepMeter1;
    private ImageButton sleepMeter2;
    private ImageButton sleepMeter3;
    private ImageButton sleepMeter4;
    private ImageButton sleepMeter5;
    private ImageButton sleepMeter6;
    private ImageButton sleepMeter7;
    private ImageButton sleepMeter8;
    private ImageButton sleepMeter9;
    private ImageButton sleepMeter10;
    private ImageButton sleepMeter11;
    private ImageButton sleepMeter12;
    private ImageButton sleepMeter13;
    private ImageButton sleepMeter14;
    private ImageButton sleepMeter15;
    private TextView sleepHoursText;
    private ImageView sleepFace;
    private ImageButton sleepHappyButton;
    private ImageButton sleepNeutralButton;
    private ImageButton sleepSadButton;
    private SeekBar wakeUpMoodBar;

}