package com.example.mentalhealthapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DailyLogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dailylog);

        getSupportActionBar().hide();
        connectingXMLViews();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.person_page:
                        navigateToProfileScreen();
                        return true;
                    case R.id.dailyLog_page:
                        System.out.println("Already in daily log page.");
                        return true;
                    case R.id.star_page:
                        navigateToRecordScreen();
                        return true;
                }
                return false;
            }
        });

        //OnClickListeners for the cards
        journalCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("journal card clicked");
                navigateToJournalScreen();
            }
        });

        moodTrackerCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("mood card clicked");
                navigateToMoodScreen();
            }
        });
        hydrationTrackerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hydration card clicked");
                navigateToHydrationScreen();
            }
        });
        sleepTrackerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("sleep card clicked");
                navigateToSleepScreen();
            }
        });
    }


    private void navigateToDailyLogScreen() {
        Intent intent = new Intent(this, DailyLogActivity.class);
        startActivity(intent);
    }
    private void navigateToRecordScreen() {
        Intent intent = new Intent(this, RecordActivity.class);
        startActivity(intent);
    }
    private void navigateToJournalScreen() {
        Intent intent = new Intent(this, JournalActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    private void navigateToMoodScreen() {
        Intent intent = new Intent(this, MoodActivity.class);
        startActivity(intent);
    }
    private void navigateToHydrationScreen() {
        Intent intent = new Intent(this, HydrationActivity.class);
        startActivity(intent);
    }
    private void navigateToSleepScreen() {
        Intent intent = new Intent(this, SleepActivity.class);
        startActivity(intent);
    }


    private void connectingXMLViews(){
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        journalCardView = findViewById(R.id.journal_card);
        moodTrackerCard = findViewById(R.id.moodTracker_card);
        hydrationTrackerCard = findViewById(R.id.hydrationTracker_card);
        sleepTrackerCard = findViewById(R.id.sleepTracker_card);
    }

    // XMLs
    private CardView journalCardView;
    private CardView moodTrackerCard;
    private CardView hydrationTrackerCard;
    private CardView sleepTrackerCard;
    private BottomNavigationView bottomNavigationView;
}