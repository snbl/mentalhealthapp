package com.example.mentalhealthapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class JournalRecordsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_records);

        getSupportActionBar().hide();
        connectingXMLViews();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.person_page:
                        navigateToProfileScreen();
                        return true;
                    case R.id.dailyLog_page:
                        navigateToDailyLogScreen();
                        return true;
                    case R.id.star_page:
                        System.out.println("Already in star page.");
                        return true;
                }
                return false;
            }
        });
    }

    private void navigateToDailyLogScreen() {
        Intent intent = new Intent(this, DailyLogActivity.class);
        startActivity(intent);
    }
    private void navigateToRecordScreen() {
        Intent intent = new Intent(this, RecordActivity.class);
        startActivity(intent);
    }
    private void navigateToProfileScreen() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    private void connectingXMLViews(){
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
    }

    // XMLs
    private BottomNavigationView bottomNavigationView;
}